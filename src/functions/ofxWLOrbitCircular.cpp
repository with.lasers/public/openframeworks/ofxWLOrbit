#include "ofxWLOrbitCircular.h"

using namespace ofxWithLasers::Orbits;

Circular::Circular(Options o) {

  //Setup coeeficients to auto update
  _coeffcients = make_shared<Coeffcients>(true, o.base, o.res, o.frameRate);

  //Add a coeffcient for theta (rotational angle in polar coordiantes)
  _coeffcients->set(Alpha::THETA, make_shared<Math::Coeffcient>(o.theta, o.dTheta, Limits::TWO_PI_BOUNDS, Limits::OOB_WRAP));

  //TODO:
  //Intertesting variotion could be to add radius as a coeffcient as well, and then allow for animation of that as well
  _radius = o.radius;

//  setCentre(o.centre);
  _setupRotation(o.rotate);

}



glm::vec3 Circular::_calculate() {
  
  //nice and easy this one - the autoupdate coefficents will be updating and wrapping theta over 0 -> TWO_PI
  //so we can just set the position using this and the radius;
//  s.set(_coeffcients->get(Alpha::THETA)->value(), _radius);

  return glm::vec3(0.0f);
  
  
}






