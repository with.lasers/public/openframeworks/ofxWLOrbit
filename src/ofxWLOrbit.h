/*

  ofxWLOrbit - add on for defining orbiting objects which can be used in other
  functions

  WithLasers::Orbit - base class which can be used to define a function which
  will return a point
                    - in three dimensional space, and be animated over time



*/

#pragma once

// Open Frameworks Includes
#include "ofMain.h"

// Add on Includes
#include "ofxWLCoeffcients.h"

// Local Includes

namespace ofxWithLasers {

  class Orbit {

  public:
    typedef shared_ptr<Orbit> Ptr;

    glm::vec3 update();

  protected:

    void _setupRotation(glm::vec3 r);

    virtual glm::vec3 _calculate() = 0;

    Math::Coeffcients::Ptr _coeffcients;

  private:

    glm::mat4 _transform;   //Transformation matrix to apply to the 2D orbit

    void _applyTransform(glm::vec3& p);

  };

} // namespace ofxWithLasers
